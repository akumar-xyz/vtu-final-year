// Copyright (c) 2016-present Mattermost, Inc. All Rights Reserved.
// See License.txt for license information.

package app

import (
	// "bytes"
	// b64 "encoding/base64"
	"fmt"
	// "hash/fnv"
	// "image"
	// "image/color"
	// "image/draw"
	// _ "image/gif"
	// _ "image/jpeg"
	// "image/png"
	// "io"
	// "io/ioutil"
	// "mime/multipart"
	// "net/http"
	// "path/filepath"
	// "strconv"
	// "strings"

	// "github.com/disintegration/imaging"
	// "github.com/golang/freetype"
	// "github.com/golang/freetype/truetype"
	// "github.com/mattermost/mattermost-server/einterfaces"
	"github.com/mattermost/mattermost-server/mlog"
	"github.com/mattermost/mattermost-server/model"
	// "github.com/mattermost/mattermost-server/utils"
)

func (a *App) GetVenueList() ([]*model.Venue, *model.AppError) {
	result := <-a.Srv.Store.Venue().GetAllVenues()
	if result.Err != nil {
		return nil, result.Err
	}
	return result.Data.([]*model.Venue), nil
}

func (a *App) CreateVenue(venue *model.Venue) (*model.Venue, *model.AppError) {
	// venue.MakeNonNil()

	result := <-a.Srv.Store.Venue().Save(venue)

	if result.Err != nil {
		mlog.Error(fmt.Sprintf("Couldn't save the venue err=%v", result.Err))
		return nil, result.Err
	}


	return result.Data.(*model.Venue), nil
}

func (a *App) IsPossible(event *model.Event) ( bool ){
	result := <-a.Srv.Store.Venue().IsFreeAt(event.StartTime, event.EndTime, event.VenueID)
	if result.Data == nil {
		return true
	} else {
		return false
	}
}
