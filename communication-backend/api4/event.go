// Copyright (c) 2017-present Mattermost, Inc. All Rights Reserved.
// See License.txt for license information.

package api4

import (
	// "fmt"
	// "io"
	// "io/ioutil"
	"net/http"
	// "strconv"
	// "time"

	// "github.com/mattermost/mattermost-server/app"
	// "github.com/mattermost/mattermost-server/mlog"
	"github.com/mattermost/mattermost-server/model"
)

func (api *API) InitEvent () {
	api.BaseRoutes.Event.Handle("/all", api.ApiHandler(ListAllEvents)).Methods("GET")
	api.BaseRoutes.Event.Handle("/upcoming", api.ApiHandler(listEvents)).Methods("GET")
	api.BaseRoutes.Event.Handle("/add", api.ApiHandler(AddEvent)).Methods("POST")
	api.BaseRoutes.Event.Handle("/{event_id:[A-Za-z0-9]+}", api.ApiHandler(EventById)).Methods("GET")

	// api.BaseRoutes.Users.Handle("", api.ApiHandler(createUser)).Methods("POST")
	// api.BaseRoutes.Users.Handle("", api.ApiSessionRequired(getUsers)).Methods("GET")
}

func listEvents(c *Context, w http.ResponseWriter, r *http.Request) {

	if c.Err != nil {
		return
	}

	// No permission check required

	var venue []*model.Event
	var err *model.AppError

	if venue, err = c.App.GetEventList(); err != nil {
		c.Err = err
		return
	}

	c.App.UpdateLastActivityAtIfNeeded(c.Session)
	w.Header().Set(model.HEADER_ETAG_SERVER, "etag")
	w.Write([]byte(model.EventListToJson(venue)))
}

func ListAllEvents(c *Context, w http.ResponseWriter, r *http.Request) {

	if c.Err != nil {
		return
	}

	// No permission check required

	var venue []*model.Event
	var err *model.AppError

	if venue, err = c.App.GetAllEventList(); err != nil {
		c.Err = err
		return
	}

	c.App.UpdateLastActivityAtIfNeeded(c.Session)
	w.Header().Set(model.HEADER_ETAG_SERVER, "etag")
	w.Write([]byte(model.EventListToJson(venue)))
}

func AddEvent(c *Context, w http.ResponseWriter, r *http.Request) {
	// c.RequireUserId()
	event := model.EventFromJson(r.Body)

	event.OrganizerID = c.Session.UserId
	if event == nil {
		c.SetInvalidParam("event")
		return
	}

	// tokenId := r.URL.Query().Get("t")
	// inviteId := r.URL.Query().Get("iid")

	// No permission check required

	var revent *model.Event
	var err *model.AppError

	if revent, err = c.App.CreateEvent(event); err != nil {
		c.Err = err
		return
	}

	w.WriteHeader(http.StatusCreated)
	w.Write([]byte(revent.ToJson()))


	// if len(tokenId) > 0 {
	// 	ruser, err = c.App.CreateUserWithToken(user, tokenId)
	// } else if len(inviteId) > 0 {
	// 	ruser, err = c.App.CreateUserWithInviteId(user, inviteId)
	// } else if c.IsSystemAdmin() {
	// 	ruser, err = c.App.CreateUserAsAdmin(user)
	// } else {
	// }
}

func EventById(c *Context, w http.ResponseWriter, r *http.Request) {

	if c.Err != nil {
		return
	}

	// No permission check required

	var event *model.Event
	var err *model.AppError
	// string EventID = c.Params.EventId
	if event, err = c.App.GetEventById (c.Params.EventId); err != nil {
		c.Err = err
		return
	}

	c.App.UpdateLastActivityAtIfNeeded(c.Session)
	w.Header().Set(model.HEADER_ETAG_SERVER, "etag")
	//EventToJson(?)
	w.Write([]byte(event.ToJson()))
}

