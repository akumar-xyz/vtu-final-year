#!/bin/env python

from flask import Flask, Response, request
from flask_restful import Api, Resource, reqparse
import mysql.connector as mariadb
import json
import datetime
from profanityfilter import ProfanityFilter

app = Flask(__name__)
api = Api(app)
parser = reqparse.RequestParser()


mydb = mariadb.connect( host="172.17.0.1", user="root", passwd="mostest", database="notices" )

def dtconvert(o):
    if isinstance(o, datetime.datetime):
        timestamp = (o - datetime.datetime(1970, 1, 1)) / datetime.timedelta(seconds=1)
        return timestamp

class Board(Resource):
    def get(self):
        mycursor = mydb.cursor(dictionary=True)
        stmt = """
SELECT  *
FROM    notices
WHERE Approved = 1
        """
# WHERE   PostedAt >= CURTIME() - INTERVAL 30 DAY 
        mycursor.execute(stmt)
        myresult = mycursor.fetchall()
        mycursor.close()
        return Response( json.dumps(myresult, default=dtconvert) ,mimetype='application/json')

class All(Resource):
    def get(self):
        mycursor = mydb.cursor(dictionary=True)
        stmt = """
SELECT  *
FROM    notices
        """
# WHERE   PostedAt >= CURTIME() - INTERVAL 30 DAY 
        mycursor.execute(stmt)
        myresult = mycursor.fetchall()
        mycursor.close()
        return json.dumps(myresult, default=dtconvert), 200

class Moderate(Resource):
    def get(self, ID):
        mycursor = mydb.cursor()
        stmt = """
UPDATE notices 
SET `Approved` = NOT `Approved`
WHERE Id = %s 
        """ % (ID)
        mycursor.execute(stmt)
        mydb.commit()
        return 200

class By_Id(Resource):
    def get(self, ID):
        mycursor = mydb.cursor(dictionary=True)
        stmt = """
SELECT  *
FROM    notices
WHERE   Id = %s
        """%(ID)
        mycursor.execute(stmt)
        myresult = mycursor.fetchall()
        mycursor.close()
        return json.dumps(myresult, default = dtconvert), 299

class submit(Resource):
    def post(self):

        json_data = request.get_json(force=True)

        try:
            title = json_data['title']
            username = "Foo"
            designation = "Administrator"
            content = json_data['content']
            pf = ProfanityFilter()
            clean_content = pf.censor(content)
            hashtags = json_data['hashtags']
        except:
            return {
                    'status' : 'failed. incomplete arguments'
            }

        stmt = """
INSERT INTO notices ( Title, Username, Designation, Content, PostedAt, HashTags, Approved )
VALUES (
%s, %s,%s, %s, now(), %s, 0 
)
"""
        mycursor = mydb.cursor(dictionary=True)
        mycursor.execute(stmt, (title, username, designation, clean_content, hashtags))
        mydb.commit()
        mycursor.close()

        if pf.is_clean( content ):
            return {
                'status': 'sucessfully submitted',
                'title': title
            }
        else :
            return {
                'status': 'sucessfully submitted',
                'title': title,
                'Warning' : 'Profanity found. You will be reported.'
            }



api.add_resource(Board, "/notice")
api.add_resource(All, "/notice/all")
api.add_resource(Moderate, "/moderate/<ID>")
api.add_resource(By_Id, "/notices/<ID>")
api.add_resource(submit, "/submit")

app.run(debug=True, host='0.0.0.0')
