package io.github.abhishekwl.alapp.Models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Subject implements Parcelable {

    private ArrayList<Post> postArrayList;
    private String rootPostId;

    public Subject(ArrayList<Post> postArrayList) {
        this.postArrayList = postArrayList;
    }

    public String getSubjectName() {
        if (postArrayList==null || postArrayList.size()==0) return null;
        else return postArrayList.get(0).getSubject();
    }

    public ArrayList<Post> getPostArrayList() {
        return postArrayList;
    }

    public void setPostArrayList(ArrayList<Post> postArrayList) {
        this.postArrayList = postArrayList;
    }

    public String getRootPostId() {
        return postArrayList.get(0).getRootId();
    }

    @Override
    public String toString() {
        return "Subject{" +
                "postArrayList=" + postArrayList +
                ", rootPostId='" + rootPostId + '\'' +
                '}';
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.postArrayList);
        dest.writeString(this.rootPostId);
    }

    private Subject(Parcel in) {
        this.postArrayList = in.createTypedArrayList(Post.CREATOR);
        this.rootPostId = in.readString();
    }

    public static final Parcelable.Creator<Subject> CREATOR = new Parcelable.Creator<Subject>() {
        @Override
        public Subject createFromParcel(Parcel source) {
            return new Subject(source);
        }

        @Override
        public Subject[] newArray(int size) {
            return new Subject[size];
        }
    };
}
