package io.github.abhishekwl.alapp.Fragments;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.applandeo.materialcalendarview.CalendarView;
import com.applandeo.materialcalendarview.EventDay;
import com.applandeo.materialcalendarview.exceptions.OutOfDateRangeException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Objects;

import butterknife.BindColor;
import butterknife.BindFont;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.github.abhishekwl.alapp.Activities.AddEventActivity;
import io.github.abhishekwl.alapp.Activities.MainActivity;
import io.github.abhishekwl.alapp.Models.Event;
import io.github.abhishekwl.alapp.Models.User;
import io.github.abhishekwl.alapp.Models.Venue;
import io.github.abhishekwl.alapp.R;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class EventsFragment extends Fragment {

    @BindFont(R.font.roboto_thin)
    Typeface robotoThinTypeface;
    @BindColor(R.color.colorAccent)
    int colorAccent;
    @BindView(R.id.eventsCalendarView)
    CalendarView eventsCalendarView;
    @BindView(R.id.eventsTopLinearLayout)
    LinearLayout eventsTopLinearLayout;
    @BindView(R.id.eventsAddFab)
    FloatingActionButton eventsAddFab;
    @BindString(R.string.base_server_url)
    String baseServerUrl;
    @BindView(R.id.eventsCalendarContainerCardView)
    CardView eventsCalendarContainerCardView;
    @BindView(R.id.eventsEventNameTextView)
    TextView eventsEventNameTextView;
    @BindView(R.id.eventsEventTimingsTextView)
    TextView eventsEventTimingsTextView;
    @BindView(R.id.eventsEventDescriptionTextView)
    TextView eventsEventDescriptionTextView;
    @BindView(R.id.eventsEventJoinButton)
    Button eventsEventJoinButton;
    @BindView(R.id.eventsHeaderTextView)
    TextView eventsHeaderTextView;
    @BindView(R.id.eventDetailsCardView)
    CardView eventDetailsCardView;

    private View rootView;
    private Calendar calendar;
    private Unbinder unbinder;
    private ArrayList<EventDay> eventDayArrayList = new ArrayList<>();
    private ArrayList<Event> eventArrayList = new ArrayList<>();
    private User currentUser;
    private String token;
    private OkHttpClient okHttpClient;

    public EventsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_events, container, false);
        unbinder = ButterKnife.bind(EventsFragment.this, rootView);
        initializeComponents();
        initializeViews();
        return rootView;
    }

    private void initializeComponents() {
        calendar = Calendar.getInstance();
        currentUser = ((MainActivity) Objects.requireNonNull(getActivity())).getCurrentUser();
        token = ((MainActivity) getActivity()).getToken();
        okHttpClient = new OkHttpClient();
        eventsCalendarView.setOnDayClickListener(eventDay -> {
            Calendar calendar = eventDay.getCalendar();
            Event event = retrieveEventForCalendar(calendar);
            if (event == null) eventDetailsCardView.setVisibility(View.GONE);
            else {
                eventDetailsCardView.setVisibility(View.VISIBLE);
                renderEventDetails(Objects.requireNonNull(event));
            }
        });
    }

    @SuppressLint("SetTextI18n")
    private void renderEventDetails(Event event) {
        Log.v("EVENT", event.toString());
        eventsEventNameTextView.setText(event.getName());
        eventsEventDescriptionTextView.setText(event.getDescription());
        long startTime = event.getStartTime();
        long endTime = event.getEndTime();
        String displayTimeStart = getTimeFromUnixTimeStamp(startTime);
        String displayTimeEnd = getTimeFromUnixTimeStamp(endTime);
        eventsEventTimingsTextView.setText("From ".concat(displayTimeStart).concat(" to ").concat(displayTimeEnd));
        eventsEventJoinButton.setOnClickListener(v -> new JoinChannel().execute(event));
    }

    @SuppressLint("StaticFieldLeak")
    private class JoinChannel extends AsyncTask<Event, Void, Response> {
        @Override
        protected Response doInBackground(Event... events) {
            try {
                MediaType mediaType = MediaType.parse("application/json; charset=utf-8");
                Event event = events[0];
                String eventChannelId = event.getEventChannelId();
                JSONObject channelJoinJson = new JSONObject();
                channelJoinJson.put("user_id", currentUser.getId());
                Request joinChannelRequest = new Request.Builder()
                        .addHeader("Authorization", "Bearer "+token)
                        .post(RequestBody.create(mediaType, channelJoinJson.toString()))
                        .url(baseServerUrl+"/channels/"+eventChannelId+"/members")
                        .build();
                return okHttpClient.newCall(joinChannelRequest).execute();
            } catch (JSONException | IOException ignored) { return null; }
        }

        @Override
        protected void onPostExecute(Response response) {
            super.onPostExecute(response);
            if (response.isSuccessful()) Snackbar.make(eventsCalendarView, "You have joined the channel.", Snackbar.LENGTH_LONG).show();
        }
    }

    private String getTimeFromUnixTimeStamp(long timestamp) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp * 1000);
        int hourOfDay = calendar.get(Calendar.HOUR);
        int minutes = calendar.get(Calendar.MINUTE);
        int amOrPm = calendar.get(Calendar.AM_PM);
        return hourOfDay + ":" + minutes + " " + (amOrPm == 0 ? "AM" : "PM");
    }

    private Event retrieveEventForCalendar(Calendar calendar) {
        int dayOfMonthSelectedDate = calendar.get(Calendar.DAY_OF_MONTH);
        Log.v("DAY_OF_MONTH_SELECTED", String.valueOf(dayOfMonthSelectedDate));
        for (Event event : eventArrayList) {
            long eventStartUnixTimeStamp = event.getStartTime();
            Calendar eventCalendar = Calendar.getInstance();
            eventCalendar.setTimeInMillis(eventStartUnixTimeStamp * 1000);
            int dayOfMonthEvent = eventCalendar.get(Calendar.DAY_OF_MONTH);
            if (dayOfMonthEvent == dayOfMonthSelectedDate) return event;
        }
        return null;
    }

    private void initializeViews() {
        new FetchAllEvents().execute();
    }

    @SuppressLint("StaticFieldLeak")
    private class FetchAllEvents extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                Request allEventsRequest = new Request.Builder()
                        .addHeader("Authorization", "Bearer " + token)
                        .url(baseServerUrl + "/event/all")
                        .build();
                Response allEventsResponse = okHttpClient.newCall(allEventsRequest).execute();
                JSONArray allEventsJsonArray = new JSONArray(Objects.requireNonNull(allEventsResponse.body()).string());
                eventArrayList.clear();
                eventDayArrayList.clear();
                for (int i = 0; i < allEventsJsonArray.length(); i++) {
                    JSONObject eventJson = allEventsJsonArray.getJSONObject(i);

                    String eventId = eventJson.getString("id");
                    String venueId = eventJson.getString("venue_id");
                    String eventName = eventJson.getString("event_name");
                    String eventDescription = eventJson.getString("description");
                    String eventType = eventJson.getString("type");
                    long eventStartTime = eventJson.getLong("start_time");
                    long eventEndTime = eventJson.getLong("end_time");
                    String eventChannelId = eventJson.getString("event_channel_id");

                    Request venueRequest = new Request.Builder()
                            .addHeader("Authorization", "Bearer "+token)
                            .url(baseServerUrl+"/venue/"+venueId)
                            .build();
                    Response venueResponse = okHttpClient.newCall(venueRequest).execute();
                    JSONObject venueJson = new JSONObject(Objects.requireNonNull(venueResponse.body()).string());
                    String venueName = venueJson.getString("name");
                    String venueDisplayName = venueJson.getString("display_name");
                    String venueDescription = venueJson.getString("description");
                    String venueType = venueJson.getString("type");
                    Venue venue = new Venue(venueId, venueName, venueDescription, venueType);

                    eventArrayList.add(new Event(eventId, eventName, venue, eventStartTime, eventEndTime, eventDescription, eventType, eventChannelId));
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTimeInMillis(eventStartTime * 1000);
                    eventDayArrayList.add(new EventDay(calendar, R.drawable.ic_event_note_black_24dp));
                }
            } catch (IOException | JSONException e) {
                return null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            eventsCalendarView.setEvents(eventDayArrayList);
            try {
                eventsCalendarView.setDate(Calendar.getInstance());
            } catch (OutOfDateRangeException ignored) { }
        }
    }

    @OnClick(R.id.eventsAddFab)
    public void onAddEventButtonPress() {
        Intent addEventActivityIntent = new Intent(rootView.getContext(), AddEventActivity.class);
        addEventActivityIntent.putExtra("USER", currentUser);
        addEventActivityIntent.putExtra("TOKEN", token);
        rootView.getContext().startActivity(addEventActivityIntent);
    }

    @Override
    public void onStart() {
        super.onStart();
        unbinder = ButterKnife.bind(EventsFragment.this, rootView);
        new FetchAllEvents().execute();
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }

}
