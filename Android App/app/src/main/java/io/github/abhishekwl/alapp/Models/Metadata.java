package io.github.abhishekwl.alapp.Models;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

public class Metadata implements Parcelable {

    private String fileHash;
    private String rawFileData;
    private String contentType;
    private Date fileCreatedAtDate;
    private long fileSize;
    private Bitmap fileBitmap;

    public Metadata(String rawFileData, String contentType, Date fileCreatedAtDate, long fileSize, Bitmap fileBitmap, String fileHash) {
        this.rawFileData = rawFileData;
        this.contentType = contentType;
        this.fileCreatedAtDate = fileCreatedAtDate;
        this.fileSize = fileSize;
        this.fileBitmap = fileBitmap;
        this.fileHash = fileHash;
    }

    private Metadata(Parcel in) {
        fileHash = in.readString();
        rawFileData = in.readString();
        contentType = in.readString();
        fileSize = in.readLong();
        fileBitmap = in.readParcelable(Bitmap.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(fileHash);
        dest.writeString(rawFileData);
        dest.writeString(contentType);
        dest.writeLong(fileSize);
        dest.writeParcelable(fileBitmap, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Metadata> CREATOR = new Creator<Metadata>() {
        @Override
        public Metadata createFromParcel(Parcel in) {
            return new Metadata(in);
        }

        @Override
        public Metadata[] newArray(int size) {
            return new Metadata[size];
        }
    };

    public String getRawFileData() {
        return rawFileData;
    }

    public void setRawFileData(String rawFileData) {
        this.rawFileData = rawFileData;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public Date getFileCreatedAtDate() {
        return fileCreatedAtDate;
    }

    public void setFileCreatedAtDate(Date fileCreatedAtDate) {
        this.fileCreatedAtDate = fileCreatedAtDate;
    }

    public long getFileSize() {
        return fileSize;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    public Bitmap getFileBitmap() {
        return fileBitmap;
    }

    public void setFileBitmap(Bitmap fileBitmap) {
        this.fileBitmap = fileBitmap;
    }

    public String getFileHash() {
        return fileHash;
    }

    public void setFileHash(String fileHash) {
        this.fileHash = fileHash;
    }

    @Override
    public String toString() {
        return "Metadata{" +
                "fileHash='" + fileHash + '\'' +
                ", rawFileData='" + rawFileData + '\'' +
                ", contentType='" + contentType + '\'' +
                ", fileCreatedAtDate=" + fileCreatedAtDate +
                ", fileSize=" + fileSize +
                ", fileBitmap=" + fileBitmap +
                '}';
    }
}
