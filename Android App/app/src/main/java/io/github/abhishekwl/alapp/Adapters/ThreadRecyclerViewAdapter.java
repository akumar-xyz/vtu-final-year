package io.github.abhishekwl.alapp.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindColor;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.abhishekwl.alapp.Models.Post;
import io.github.abhishekwl.alapp.Models.User;
import io.github.abhishekwl.alapp.R;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import ru.noties.markwon.Markwon;

public class ThreadRecyclerViewAdapter extends RecyclerView.Adapter<ThreadRecyclerViewAdapter.ThreadViewHolder> {

    private ArrayList<Post> postArrayList;
    private OkHttpClient okHttpClient;
    private User currentUser;
    private String token;

    public ThreadRecyclerViewAdapter(ArrayList<Post> postArrayList, User currentUser, String token) {
        this.postArrayList = postArrayList;
        this.okHttpClient = new OkHttpClient();
        this.currentUser = currentUser;
        this.token = token;
    }

    @NonNull
    @Override
    public ThreadRecyclerViewAdapter.ThreadViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View threadListItemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.thread_list_item, viewGroup, false);
        return new ThreadViewHolder(threadListItemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ThreadRecyclerViewAdapter.ThreadViewHolder threadViewHolder, int i) {
        Post post = postArrayList.get(i);
        threadViewHolder.bind(threadViewHolder.itemView.getContext(), post);
    }

    @Override
    public int getItemCount() {
        return postArrayList.size();
    }

    class ThreadViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.threadListItemAuthorNameTextView)
        TextView postAuthorNameTextView;
        @BindView(R.id.threadListItemMarkdownView)
        TextView postMarkdownView;
        @BindString(R.string.base_server_url)
        String baseServerUrl;
        @BindView(R.id.threadListItemCardView)
        CardView cardView;
        @BindColor(R.color.colorAccent)
        int colorAccent;
        @BindColor(R.color.colorTextDark)
        int colorTextDark;

        ThreadViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(Context context, Post post) {
            Markwon.setMarkdown(postMarkdownView, post.getMessage());
            //TODO: Fetch Post Author
            String postAuthorId = post.getUserId();
            new FetchUser().execute(postAuthorId);
        }

        @SuppressLint("StaticFieldLeak")
        private class FetchUser extends AsyncTask<String, Void, String> {

            private String uid;

            @Override
            protected String doInBackground(String... strings) {
                try {
                    uid = strings[0];
                    Request userRequest = new Request.Builder()
                            .addHeader("Authorization", "Bearer "+token)
                            .url(baseServerUrl+"/users/"+uid)
                            .build();
                    Response userResponse = okHttpClient.newCall(userRequest).execute();
                    JSONObject userJson = new JSONObject(Objects.requireNonNull(userResponse.body()).string());
                    return userJson.getString("username");
                } catch (IOException | JSONException ignored) {
                    return null;
                }
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                if (s!=null && !s.isEmpty()) {
                    postAuthorNameTextView.setText("@".concat(s));
                }
                if (uid.equals(currentUser.getId())) {
                    postAuthorNameTextView.setGravity(Gravity.END);
                    postMarkdownView.setTextColor(colorTextDark);
                    cardView.setCardBackgroundColor(Color.parseColor("#EEEEEE"));
                }
                postAuthorNameTextView.setTextColor(colorAccent);
            }
        }
    }
}
