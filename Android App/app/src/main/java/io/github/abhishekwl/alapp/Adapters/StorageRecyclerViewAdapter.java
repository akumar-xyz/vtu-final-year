package io.github.abhishekwl.alapp.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.abhishekwl.alapp.Models.StorageItem;
import io.github.abhishekwl.alapp.R;

public class StorageRecyclerViewAdapter extends RecyclerView.Adapter<StorageRecyclerViewAdapter.StorageViewHolder> {

    private ArrayList<StorageItem> storageItemArrayList;

    public StorageRecyclerViewAdapter(ArrayList<StorageItem> storageItemArrayList) {
        this.storageItemArrayList = storageItemArrayList;
    }

    @NonNull
    @Override
    public StorageRecyclerViewAdapter.StorageViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.storage_list_item, viewGroup, false);
        return new StorageViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull StorageRecyclerViewAdapter.StorageViewHolder storageViewHolder, int i) {
        StorageItem storageItem = storageItemArrayList.get(i);
        storageViewHolder.render(storageViewHolder.itemView.getContext(), storageItem);
    }

    @Override
    public int getItemCount() {
        return storageItemArrayList.size();
    }

    class StorageViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.storageListItemFileNameTextView)
        TextView fileNameTextView;
        @BindView(R.id.storageListItemImageView)
        ImageView fileImageView;

        StorageViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void render(Context context, StorageItem storageItem) {
            fileNameTextView.setText(storageItem.getFileName());
        }
    }
}
