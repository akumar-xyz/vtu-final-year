package io.github.abhishekwl.alapp.Fragments;


import android.annotation.SuppressLint;
import android.app.DownloadManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;

import net.glxn.qrgen.android.QRCode;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;
import io.github.abhishekwl.alapp.Activities.MainActivity;
import io.github.abhishekwl.alapp.Activities.SignInActivity;
import io.github.abhishekwl.alapp.Activities.WebViewActivity;
import io.github.abhishekwl.alapp.Adapters.IpfsGridViewAdapter;
import io.github.abhishekwl.alapp.Helpers.RecyclerItemClickListener;
import io.github.abhishekwl.alapp.Models.Metadata;
import io.github.abhishekwl.alapp.Models.User;
import io.github.abhishekwl.alapp.R;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.DOWNLOAD_SERVICE;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {

    @BindView(R.id.profileImageView)
    CircleImageView profileImageView;
    @BindView(R.id.profileTopLinearLayout)
    LinearLayout profileTopLinearLayout;
    @BindString(R.string.base_server_url)
    String baseServerUrl;
    @BindString(R.string.global_team_id)
    String teamId;
    @BindView(R.id.profileStorageRecyclerView)
    RecyclerView profileStorageRecyclerView;
    @BindString(R.string.ipfs_server_url)
    String ipfsServerUrl;
    @BindString(R.string.ipfs_root_server_url)
    String ipfsRootServerUrl;
    @BindView(R.id.profileStorageProgressBar)
    ProgressBar profileStorageProgressBar;
    @BindString(R.string.global_team_id)
    String globalTeamId;
    @BindView(R.id.profileCardNameTextView)
    TextView profileCardNameTextView;
    @BindView(R.id.profileCardDesignationTextView)
    TextView profileCardDesignationTextView;
    @BindView(R.id.profileCardKsitLogoImageView)
    ImageView profileCardKsitLogoImageView;
    @BindView(R.id.profileCardProfilePictureImageView)
    ImageView profileCardProfilePictureImageView;
    @BindView(R.id.profileAddToStorageImageView)
    ImageView profileAddToStorageImageView;
    @BindView(R.id.collegeIdCardView)
    CardView collegeIdCardView;
    @BindView(R.id.profileCardBarCodeImageView)
    ImageView profileCardBarCodeImageView;

    private User currentUser;
    private String token;
    private View rootView;
    private Unbinder unbinder;
    private static final int PICKFILE_REQUEST_CODE = 986;
    private OkHttpClient okHttpClient;
    private ArrayList<String> hashesArrayList = new ArrayList<>();
    private ArrayList<Metadata> metadataArrayList = new ArrayList<>();
    private IpfsGridViewAdapter ipfsGridViewAdapter;
    private MaterialDialog materialDialog;
    private DownloadManager downloadmanager;

    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        unbinder = ButterKnife.bind(ProfileFragment.this, rootView);
        initializeComponents();
        initializeViews();
        return rootView;
    }

    private void initializeComponents() {
        currentUser = ((MainActivity) Objects.requireNonNull(getActivity())).getCurrentUser();
        Log.v("USER", currentUser.toString());
        token = ((MainActivity) getActivity()).getToken();
        okHttpClient = new OkHttpClient();
        ipfsGridViewAdapter = new IpfsGridViewAdapter(metadataArrayList, currentUser);
        downloadmanager = (DownloadManager) rootView.getContext().getSystemService(DOWNLOAD_SERVICE);
    }

    private void initializeViews() {
        Glide.with(rootView.getContext()).load(currentUser.getImageBitmap()).into(profileImageView);
        Glide.with(rootView.getContext()).load(R.drawable.ksit_logo).into(profileCardKsitLogoImageView);
        Glide.with(rootView.getContext()).load(currentUser.getImageBitmap()).into(profileCardProfilePictureImageView);
        profileCardNameTextView.setText(currentUser.getUserName());
        profileCardDesignationTextView.setText("Student");
        initializeRecyclerView();
        profileCardBarCodeImageView.setImageBitmap(QRCode.from(currentUser.getUsn()).bitmap());
        new FetchUserHashes().execute();
    }

    private void initializeRecyclerView() {
        profileStorageRecyclerView.setLayoutManager(new GridLayoutManager(rootView.getContext(), 3));
        profileStorageRecyclerView.setHasFixedSize(true);
        profileStorageRecyclerView.setItemAnimator(new DefaultItemAnimator());
        profileStorageRecyclerView.setAdapter(ipfsGridViewAdapter);
        profileStorageRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(rootView.getContext(), (view, position) -> {
            Metadata metadata = metadataArrayList.get(position);
            materialDialog = new MaterialDialog.Builder(rootView.getContext())
                    .title(R.string.app_name)
                    .content("Choose an action")
                    .items(R.array.ipfs_menu)
                    .titleColorRes(android.R.color.black)
                    .itemsCallback((dialog, itemView, position1, text) -> {
                        if (text.toString().equalsIgnoreCase("View Full Size"))
                            viewFullSizeFile(metadata);
                        else if (text.toString().equalsIgnoreCase("Download"))
                            downloadFile(metadata);
                        else if (text.toString().equalsIgnoreCase("Share")) shareFile(metadata);
                    })
                    .show();
        }));
    }

    private void shareFile(Metadata metadata) {
        new ShareFile().execute(metadata);
    }

    public Bitmap retrieveBitmapFromView(View v) {
        Bitmap b = Bitmap.createBitmap(v.getLayoutParams().width, v.getLayoutParams().height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        v.layout(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
        v.draw(c);
        return b;
    }

    @SuppressLint("StaticFieldLeak")
    private class ShareFile extends AsyncTask<Metadata, Void, ArrayList<String>> {

        private String fileHash;
        private ArrayList<String> channelsDisplayNamesArrayList;

        @Override
        protected ArrayList<String> doInBackground(Metadata... metadata) {
            try {
                fileHash = metadata[0].getFileHash();
                Request channelsRequest = new Request.Builder()
                        .addHeader("Authorization", "Bearer " + token)
                        .url(baseServerUrl + "/users/" + currentUser.getId() + "/teams/" + globalTeamId + "/channels")
                        .build();
                Response channelsResponse = okHttpClient.newCall(channelsRequest).execute();
                JSONArray channelsJsonArray = new JSONArray(Objects.requireNonNull(channelsResponse.body()).string());
                channelsDisplayNamesArrayList = new ArrayList<>();
                ArrayList<String> finalStringsList = new ArrayList<>();
                for (int i = 0; i < channelsJsonArray.length(); i++) {
                    JSONObject channelJson = channelsJsonArray.getJSONObject(i);
                    String channelId = channelJson.getString("id");
                    String channelDisplayName = channelJson.getString("display_name");
                    channelsDisplayNamesArrayList.add(channelDisplayName);
                    finalStringsList.add(channelDisplayName + "\t" + channelId);
                }
                return finalStringsList;
            } catch (IOException | JSONException e) {
                return new ArrayList<>();
            }
        }

        @Override
        protected void onPostExecute(ArrayList<String> strings) {
            super.onPostExecute(strings);
            if (strings == null || strings.isEmpty())
                Snackbar.make(profileImageView, "No channels found", Snackbar.LENGTH_LONG).show();
            else {
                materialDialog = new MaterialDialog.Builder(rootView.getContext())
                        .title(R.string.app_name)
                        .contentColorRes(R.color.colorTextDark)
                        .content("Choose channel to share hash with")
                        .items(channelsDisplayNamesArrayList)
                        .itemsCallback((dialog, itemView, position, text) -> {
                            int selectedItemIndex = channelsDisplayNamesArrayList.indexOf(text);
                            String finalString = strings.get(selectedItemIndex);
                            int tabIndex = finalString.indexOf("\t");
                            String selectedChannelId = finalString.substring(tabIndex + 1);
                            new SharePostOnChannel().execute(selectedChannelId + "_" + fileHash);
                        })
                        .show();
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class SharePostOnChannel extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... strings) {
            try {
                String finalString = strings[0];
                String channelId = finalString.substring(0, finalString.indexOf("_"));
                String hashToBeShared = finalString.substring(finalString.indexOf("_") + 1);
                Log.v("CHANNEL_ID", channelId);
                Log.v("HASH_TO_BE", hashToBeShared);
                MediaType mediaType = MediaType.parse("application/json; charset=utf-8");
                JSONObject requestBodyJson = new JSONObject();
                requestBodyJson.put("channel_id", channelId);
                requestBodyJson.put("message", ipfsServerUrl + "/stream?hash=" + hashToBeShared);
                Request createPostRequest = new Request.Builder()
                        .addHeader("Authorization", "Bearer " + token)
                        .post(RequestBody.create(mediaType, requestBodyJson.toString()))
                        .url(baseServerUrl + "/posts")
                        .build();
                okHttpClient.newCall(createPostRequest).execute();
                return null;
            } catch (JSONException | IOException ignored) {
                return null;
            }
        }
    }

    private void downloadFile(Metadata metadata) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(ipfsServerUrl + "/stream?hash=" + metadata.getFileHash()));
        rootView.getContext().startActivity(intent);
    }

    private void viewFullSizeFile(Metadata metadata) {
        Intent webViewActivityIntent = new Intent(rootView.getContext(), WebViewActivity.class);
        webViewActivityIntent.putExtra("USER", currentUser);
        webViewActivityIntent.putExtra("TOKEN", token);
        webViewActivityIntent.putExtra("WEB_VIEW_URL", ipfsServerUrl + "/stream?hash=" + metadata.getFileHash());
        rootView.getContext().startActivity(webViewActivityIntent);
    }

    private String getRealPathFromURI(Context context, Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        try (Cursor cursor = context.getContentResolver().query(contentUri, proj, null, null, null)) {
            int column_index = Objects.requireNonNull(cursor).getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } catch (Exception e) {
            Log.v("getRealPathFromURI", e.toString());
            return "";
        }
    }

    @OnClick(R.id.profileAddToStorageImageView)
    public void onAddToStorageButtonPress() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("file/*");
        startActivityForResult(intent, PICKFILE_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICKFILE_REQUEST_CODE && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri selectedFileUri = data.getData();
            new UploadFileToIpfs().execute(selectedFileUri);
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class UploadFileToIpfs extends AsyncTask<Uri, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            materialDialog = new MaterialDialog.Builder(rootView.getContext())
                    .title(R.string.app_name)
                    .content("Uploading file to IPFS...")
                    .titleColorRes(android.R.color.black)
                    .contentColorRes(R.color.colorTextDark)
                    .progress(true, 0)
                    .show();
        }

        @Override
        protected String doInBackground(Uri... uris) {
            try {
                Uri selectedFileUri = uris[0];
                File fileToBeUploaded = new File(getRealPathFromURI(rootView.getContext(), selectedFileUri));
                ContentResolver contentResolver = rootView.getContext().getContentResolver();
                String mimeType = contentResolver.getType(selectedFileUri);
                RequestBody fileUploadRequestBody = new MultipartBody.Builder()
                        .setType(MultipartBody.FORM)
                        .addFormDataPart("file", selectedFileUri.getLastPathSegment(), RequestBody.create(MediaType.parse((Objects.requireNonNull(mimeType))), fileToBeUploaded))
                        .build();
                Request fileUploadRequest = new Request.Builder()
                        .post(fileUploadRequestBody)
                        .url(ipfsServerUrl + "/upload?id=" + currentUser.getId())
                        .build();
                Response fileUploadResponse = okHttpClient.newCall(fileUploadRequest).execute();
                JSONObject fileIpfsUploadJson = new JSONObject(Objects.requireNonNull(fileUploadResponse.body()).string());
                String fileHash = fileIpfsUploadJson.getString("hash");
                Request addHashToUserRequest = new Request.Builder()
                        .url(ipfsServerUrl + "/users/add/" + currentUser.getId() + "?hash=" + fileHash)
                        .build();
                Response userHashAdditionResponse = okHttpClient.newCall(addHashToUserRequest).execute();
                return Objects.requireNonNull(userHashAdditionResponse.body()).string();
            } catch (IOException | JSONException ignored) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (materialDialog.isShowing()) materialDialog.dismiss();
            try {
                if (!s.trim().equalsIgnoreCase("Hash already exists")) {
                    JSONObject userHashesJson = new JSONObject(s);
                    JSONArray userHashesJsonArray = userHashesJson.getJSONArray("hashes");
                    hashesArrayList.clear();
                    for (int i = 0; i < userHashesJsonArray.length(); i++)
                        hashesArrayList.add(userHashesJsonArray.getString(i));
                    Snackbar.make(profileImageView, "Refreshing", Snackbar.LENGTH_LONG).show();
                    new FetchFilesFromHashes().execute();
                } else
                    Toast.makeText(rootView.getContext(), "Hash already exists, referencing hash\nWe just saved storage space for you :)", Toast.LENGTH_SHORT).show();
            } catch (JSONException ignored) {
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class FetchUserHashes extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            profileStorageProgressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                Request userHashesRequest = new Request.Builder()
                        .url(ipfsServerUrl + "/users/" + currentUser.getId())
                        .build();
                Response userHashesResponse = okHttpClient.newCall(userHashesRequest).execute();
                JSONObject userHashesRootJson = new JSONObject(Objects.requireNonNull(userHashesResponse.body()).string());
                JSONArray userHashesJsonArray = userHashesRootJson.getJSONArray("hashes");
                hashesArrayList.clear();
                for (int i = 0; i < userHashesJsonArray.length(); i++) {
                    if (userHashesJsonArray.getString(i) != null && !TextUtils.isEmpty(userHashesJsonArray.getString(i)) && !userHashesJsonArray.getString(i).equalsIgnoreCase("null"))
                        hashesArrayList.add(userHashesJsonArray.getString(i));
                }
            } catch (IOException | JSONException ignored) {
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            new FetchFilesFromHashes().execute();
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class FetchFilesFromHashes extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            metadataArrayList.clear();
            for (String hash : hashesArrayList) {
                try {
                    Request fileRequest = new Request.Builder()
                            .url(ipfsServerUrl + "/stream?hash=" + hash)
                            .build();
                    Bitmap bitmap = null;
                    Response fileResponse = okHttpClient.newCall(fileRequest).execute();
                    String rawFileData = Objects.requireNonNull(fileResponse.body()).string();
                    String fileContentType = fileResponse.header("content-type");
                    long fileSize = Long.parseLong(Objects.requireNonNull(fileResponse.header("content-length")));
                    String fileCreatedAtDateString = fileResponse.header("date");
                    Metadata metadata = new Metadata(rawFileData, fileContentType, null, fileSize, null, hash);
                    if (Objects.requireNonNull(fileContentType).trim().toLowerCase().contains("image")) {
                        InputStream inputStream = fileResponse.body().byteStream();
                        bitmap = BitmapFactory.decodeStream(inputStream);
                    } else if (fileContentType.trim().toLowerCase().contains("video")) {
                        bitmap = retrieveVideoFrameFromVideo(ipfsServerUrl + "/stream?hash=" + hash);
                    }
                    metadata.setFileBitmap(bitmap);
                    metadataArrayList.add(metadata);
                } catch (Throwable ignored) {
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (profileStorageProgressBar.getVisibility() == View.VISIBLE)
                profileStorageProgressBar.setVisibility(View.GONE);
            ipfsGridViewAdapter.notifyDataSetChanged();
        }

        Bitmap retrieveVideoFrameFromVideo(String videoPath) throws Throwable {
            Bitmap bitmap = null;
            MediaMetadataRetriever mediaMetadataRetriever = null;
            try {
                mediaMetadataRetriever = new MediaMetadataRetriever();
                mediaMetadataRetriever.setDataSource(videoPath, new HashMap<>());
                bitmap = mediaMetadataRetriever.getFrameAtTime(1000);
            } catch (Exception e) {
                e.printStackTrace();
                throw new Throwable("Exception in retrieveVideoFrameFromVideo(String videoPath)" + e.getMessage());
            } finally {
                if (mediaMetadataRetriever != null) {
                    mediaMetadataRetriever.release();
                }
            }
            return bitmap;
        }
    }

    @OnClick(R.id.collegeIdCardView)
    public void onCollegeIdCardViewButtonPress() {

    }

    @Override
    public void onStart() {
        super.onStart();
        unbinder = ButterKnife.bind(ProfileFragment.this, rootView);
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }

    public String BitMapToString(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        return Base64.encodeToString(b, Base64.DEFAULT);
    }

    public Bitmap StringToBitMap(String encodedString) {
        try {
            byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);
            return BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }

    @OnClick(R.id.profileImageView)
    public void onProfileIconPress() {
        startActivity(new Intent(getActivity(), SignInActivity.class));
        getActivity().finish();
    }
}
