package io.github.abhishekwl.alapp.Activities;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.stfalcon.chatkit.messages.MessageInput;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;

import butterknife.BindArray;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.github.abhishekwl.alapp.Adapters.ThreadRecyclerViewAdapter;
import io.github.abhishekwl.alapp.Helpers.RecyclerItemClickListener;
import io.github.abhishekwl.alapp.Models.Channel;
import io.github.abhishekwl.alapp.Models.Post;
import io.github.abhishekwl.alapp.Models.Subject;
import io.github.abhishekwl.alapp.Models.User;
import io.github.abhishekwl.alapp.R;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;

public class ThreadActivity extends AppCompatActivity {

    @BindView(R.id.threadSubjectNameTextView)
    TextView threadSubjectNameTextView;
    @BindView(R.id.threadTopLinearLayout)
    LinearLayout threadTopLinearLayout;
    @BindView(R.id.threadPostsRecyclerView)
    RecyclerView threadPostsRecyclerView;
    @BindView(R.id.threadMessageInputView)
    MessageInput threadMessageInputView;
    @BindString(R.string.base_server_url)
    String baseServerUrl;
    @BindArray(R.array.translation_languages)
    String[] translationLanguages;
    @BindString(R.string.yandex_api_key)
    String yandexApiKey;

    private Unbinder unbinder;
    private User currentUser;
    private Channel currentChannel;
    private Subject currentSubject;
    private String token;
    private OkHttpClient okHttpClient;
    private ArrayList<Post> postArrayList = new ArrayList<>();
    private MainWebSocketListener mainWebSocketListener;
    private WebSocket webSocket;
    private ThreadRecyclerViewAdapter threadRecyclerViewAdapter;
    private MaterialDialog materialDialog;
    private String destinationLanguageCode = "ms"; //ms/ta/zh

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thread);

        unbinder = ButterKnife.bind(ThreadActivity.this);
        initializeComponents();
        initializeViews();
    }

    private void initializeComponents() {
        currentUser = getIntent().getParcelableExtra("USER");
        currentChannel = getIntent().getParcelableExtra("CHANNEL");
        currentSubject = getIntent().getParcelableExtra("SUBJECT");
        token = getIntent().getStringExtra("TOKEN");
        postArrayList = currentSubject.getPostArrayList();
        okHttpClient = new OkHttpClient();
        threadRecyclerViewAdapter = new ThreadRecyclerViewAdapter(postArrayList, currentUser, token);
        Request webSocketRequest = new Request.Builder()
                .addHeader("Authorization", "Bearer "+token)
                .url(baseServerUrl+"/websocket")
                .build();
        mainWebSocketListener = new MainWebSocketListener();
        webSocket = okHttpClient.newWebSocket(webSocketRequest, mainWebSocketListener);
    }

    private void initializeViews() {
        threadSubjectNameTextView.setText("_".concat(currentSubject.getSubjectName()));
        initializeRecyclerView();
        threadMessageInputView.setInputListener(input -> {
            new CreateNewPost().execute(input.toString());
            return true;
        });
    }

    @SuppressLint("StaticFieldLeak")
    private class CreateNewPost extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... strings) {
            try {
                String message = strings[0];
                String subject = currentSubject.getSubjectName();
                String channelId = currentChannel.getId();
                MediaType mediaType = MediaType.parse("application/json; charset=utf-8");
                JSONObject requestBodyJson = new JSONObject();
                requestBodyJson.put("channel_id", channelId);
                requestBodyJson.put("message", message);
                requestBodyJson.put("subject", subject);
                Request createPostRequest = new Request.Builder()
                        .addHeader("Authorization", "Bearer "+token)
                        .post(RequestBody.create(mediaType, requestBodyJson.toString()))
                        .url(baseServerUrl+"/posts")
                        .build();
                okHttpClient.newCall(createPostRequest).execute();
            } catch (JSONException | IOException e) {
                return null;
            }
            return null;
        }
    }

    private class MainWebSocketListener extends WebSocketListener {
        private static final int NORMAL_CLOSURE_STATUS = 1000;

        @Override
        public void onOpen(WebSocket webSocket, Response response) {
            super.onOpen(webSocket, response);
            try {
                Log.v("WEBSOCKET_OPEN", Objects.requireNonNull(response.body()).string());
            } catch (IOException ignored) { }
        }

        @Override
        public void onMessage(WebSocket webSocket, String text) {
            super.onMessage(webSocket, text);
            Log.v("WEBSOCKET_RECV", text);
            handleNewMessage(text);
        }

        @Override
        public void onClosing(WebSocket webSocket, int code, String reason) {
            super.onClosing(webSocket, code, reason);
            webSocket.close(NORMAL_CLOSURE_STATUS,null);
            Log.v("WEBSOCKET_CLOSE",reason);
        }

        @Override
        public void onFailure(WebSocket webSocket, Throwable t, Response response) {
            super.onFailure(webSocket, t, response);
            Log.v("WEBSOCKET_FAIL", t.getMessage());
        }
    }

    private void handleNewMessage(String text) {
        try {
            JSONObject messageJson = new JSONObject(text);
            String event = messageJson.getString("event");
            if (event.equalsIgnoreCase("posted") && messageJson.getJSONObject("broadcast").getString("channel_id").equals(currentChannel.getId())) {
                String postString = messageJson.getJSONObject("data").getString("post");
                JSONObject postJson = new JSONObject(postString);

                String postId = postJson.getString("id");
                String rootId = postJson.getString("root_id");
                String channelId = postJson.getString("channel_id");
                String postAuthorId = postJson.getString("user_id");
                String postContent = postJson.getString("message");
                long updatedAt = postJson.getLong("update_at");
                Date updatedAtDate = new Date(updatedAt);

                Post newPost = new Post(postId, updatedAtDate, postAuthorId, channelId, postContent, rootId, currentSubject.getSubjectName(), new ArrayList<>());
                postArrayList.add(newPost);
                threadRecyclerViewAdapter.notifyDataSetChanged();
            }
        } catch (JSONException ignored) { }
    }

    private void initializeRecyclerView() {
        threadPostsRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        threadPostsRecyclerView.setHasFixedSize(true);
        threadPostsRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getApplicationContext(), (view, position) -> {
            Post selectedPost = postArrayList.get(position);
            new PerformTranslation().execute(selectedPost);
        }));
        threadPostsRecyclerView.setAdapter(threadRecyclerViewAdapter);
    }

    @SuppressLint("StaticFieldLeak")
    private class PerformTranslation extends AsyncTask<Post, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Snackbar.make(threadSubjectNameTextView, "Translating text...", Snackbar.LENGTH_SHORT).show();
        }

        @Override
        protected String doInBackground(Post... posts) {
            try {
                Post selectedPost = posts[0];
                String postMessage = selectedPost.getMessage();

                HttpUrl.Builder urlBuilder = Objects.requireNonNull(HttpUrl.parse("https://translate.yandex.net/api/v1.5/tr.json/translate")).newBuilder();
                urlBuilder.addQueryParameter("key", yandexApiKey);
                urlBuilder.addQueryParameter("text", postMessage);
                urlBuilder.addQueryParameter("lang", "en-zh");
                Request translationRequest = new Request.Builder()
                        .url(urlBuilder.build())
                        .build();
                Response translationResponse = okHttpClient.newCall(translationRequest).execute();
                JSONObject translationResponseJson = new JSONObject(Objects.requireNonNull(translationResponse.body()).string());
                JSONArray textArray = translationResponseJson.getJSONArray("text");
                String translatedText = "CHINESE (SIMPLIFIED):\n";
                for(int i=0; i<textArray.length(); i++) translatedText = translatedText.concat(textArray.getString(i)+" ");
                translatedText = translatedText.trim();

                urlBuilder = Objects.requireNonNull(HttpUrl.parse("https://translate.yandex.net/api/v1.5/tr.json/translate")).newBuilder();
                urlBuilder.addQueryParameter("key", yandexApiKey);
                urlBuilder.addQueryParameter("text", postMessage);
                urlBuilder.addQueryParameter("lang", "en-ml");
                translationRequest = new Request.Builder()
                        .url(urlBuilder.build())
                        .build();
                translationResponse = okHttpClient.newCall(translationRequest).execute();
                translationResponseJson = new JSONObject(Objects.requireNonNull(translationResponse.body()).string());
                textArray = translationResponseJson.getJSONArray("text");
                translatedText+="\n\nMALAY:\n";
                for(int i=0; i<textArray.length(); i++) translatedText = translatedText.concat(textArray.getString(i)+" ");
                translatedText = translatedText.trim();

                urlBuilder = Objects.requireNonNull(HttpUrl.parse("https://translate.yandex.net/api/v1.5/tr.json/translate")).newBuilder();
                urlBuilder.addQueryParameter("key", yandexApiKey);
                urlBuilder.addQueryParameter("text", postMessage);
                urlBuilder.addQueryParameter("lang", "en-ta");
                translationRequest = new Request.Builder()
                        .url(urlBuilder.build())
                        .build();
                translationResponse = okHttpClient.newCall(translationRequest).execute();
                translationResponseJson = new JSONObject(Objects.requireNonNull(translationResponse.body()).string());
                textArray = translationResponseJson.getJSONArray("text");
                translatedText+="\n\nTAMIL:\n";
                for(int i=0; i<textArray.length(); i++) translatedText = translatedText.concat(textArray.getString(i)+" ");

                return translatedText.trim();
            } catch (IOException | JSONException ignored) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.v("TRANSLATION", s);
            materialDialog = new MaterialDialog.Builder(ThreadActivity.this)
                    .title(R.string.app_name)
                    .content("Translated text:\n"+s)
                    .titleColor(Color.BLACK)
                    .contentColorRes(android.R.color.black)
                    .positiveText("OKAY")
                    .negativeText("DISMISS")
                    .onAny((dialog, which) -> dialog.dismiss())
                    .positiveColorRes(R.color.colorAccent)
                    .negativeColorRes(R.color.colorTextDark)
                    .show();
        }
    }

    @Override
    protected void onDestroy() {
        unbinder.unbind();
        super.onDestroy();
    }
}
