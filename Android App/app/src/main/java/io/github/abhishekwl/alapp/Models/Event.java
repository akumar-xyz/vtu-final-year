package io.github.abhishekwl.alapp.Models;

import android.os.Parcel;
import android.os.Parcelable;

public class Event implements Parcelable {

    private String id;
    private String name;
    private Venue venue;
    private long startTime;
    private long endTime;
    private String description;
    private String type;
    private String eventChannelId;

    public Event(String id, String name, Venue venue, long startTime, long endTime, String description, String type, String eventChannelId) {
        this.id = id;
        this.name = name;
        this.venue = venue;
        this.startTime = startTime;
        this.endTime = endTime;
        this.description = description;
        this.type = type;
        this.eventChannelId = eventChannelId;
    }

    public Event(String id, String name, long startTime, long endTime, String description, String type, String eventChannelId) {
        this.id = id;
        this.name = name;
        this.startTime = startTime;
        this.endTime = endTime;
        this.description = description;
        this.type = type;
        this.eventChannelId = eventChannelId;
    }

    public Event() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Venue getVenue() {
        return venue;
    }

    public void setVenue(Venue venue) {
        this.venue = venue;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeParcelable(this.venue, flags);
        dest.writeLong(this.startTime);
        dest.writeLong(this.endTime);
        dest.writeString(this.description);
        dest.writeString(this.type);
    }

    private Event(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.venue = in.readParcelable(Venue.class.getClassLoader());
        this.startTime = in.readLong();
        this.endTime = in.readLong();
        this.description = in.readString();
        this.type = in.readString();
    }

    public static final Parcelable.Creator<Event> CREATOR = new Parcelable.Creator<Event>() {
        @Override
        public Event createFromParcel(Parcel source) {
            return new Event(source);
        }

        @Override
        public Event[] newArray(int size) {
            return new Event[size];
        }
    };

    @Override
    public String toString() {
        return "Event{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", venue=" + venue +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", description='" + description + '\'' +
                ", type='" + type + '\'' +
                ", eventChannelId='" + eventChannelId + '\'' +
                '}';
    }

    public String getEventChannelId() {
        return eventChannelId;
    }

    public void setEventChannelId(String eventChannelId) {
        this.eventChannelId = eventChannelId;
    }
}
