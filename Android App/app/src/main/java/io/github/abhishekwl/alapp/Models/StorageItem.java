package io.github.abhishekwl.alapp.Models;

import android.os.Parcel;
import android.os.Parcelable;

public class StorageItem implements Parcelable {

    private String fileName;
    private String fileHash;

    public StorageItem(String fileName, String fileHash) {
        this.fileName = fileName;
        this.fileHash = fileHash;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileHash() {
        return fileHash;
    }

    public void setFileHash(String fileHash) {
        this.fileHash = fileHash;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.fileName);
        dest.writeString(this.fileHash);
    }

    private StorageItem(Parcel in) {
        this.fileName = in.readString();
        this.fileHash = in.readString();
    }

    public static final Parcelable.Creator<StorageItem> CREATOR = new Parcelable.Creator<StorageItem>() {
        @Override
        public StorageItem createFromParcel(Parcel source) {
            return new StorageItem(source);
        }

        @Override
        public StorageItem[] newArray(int size) {
            return new StorageItem[size];
        }
    };
}
