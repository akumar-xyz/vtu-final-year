package io.github.abhishekwl.alapp.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.abhishekwl.alapp.Models.Subject;
import io.github.abhishekwl.alapp.R;

public class SubjectsRecyclerViewAdapter extends RecyclerView.Adapter<SubjectsRecyclerViewAdapter.SubjectViewHolder> {

    private ArrayList<Subject> subjectArrayList;

    public SubjectsRecyclerViewAdapter(ArrayList<Subject> subjectArrayList) {
        this.subjectArrayList = subjectArrayList;
    }

    @NonNull
    @Override
    public SubjectsRecyclerViewAdapter.SubjectViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View listItemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.subject_list_item, viewGroup, false);
        return new SubjectViewHolder(listItemView);
    }

    @Override
    public void onBindViewHolder(@NonNull SubjectsRecyclerViewAdapter.SubjectViewHolder subjectViewHolder, int i) {
        Subject subject = subjectArrayList.get(i);
        subjectViewHolder.bind(subjectViewHolder.itemView.getContext(), subject);
    }

    @Override
    public int getItemCount() {
        return subjectArrayList.size();
    }

    class SubjectViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.subjectListItemSubjectNameTextView)
        TextView subjectNameTextView;
        @BindView(R.id.subjectListItemSubjectLatestMessageTextView)
        TextView subjectLatestMessageTextView;

        SubjectViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(Context context, Subject subject) {
            if(subject.getSubjectName()!=null) {
                subjectNameTextView.setText("_".concat(subject.getSubjectName()));
                subjectLatestMessageTextView.setText(subject.getPostArrayList().get(subject.getPostArrayList().size()-1).getMessage());
            }
        }
    }
}
