package io.github.abhishekwl.alapp.Models;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;

import com.stfalcon.chatkit.commons.models.IMessage;
import com.stfalcon.chatkit.commons.models.MessageContentType;

import java.util.Date;

public class Message implements Parcelable, IMessage, MessageContentType.Image {

    private String id;
    private String postContent;
    private User user;
    private String subject;
    private Date messageUpdatedAtDate;
    private String channelId;
    private String postType;
    private String fileId;
    private String fileThumbnailUrl;

    public Message(String id, String postContent, User user, Date messageUpdatedAtDate, String channelId, String postType, String fileId, String fileThumbnailUrl) {
        this.id = id;
        this.postContent = postContent.trim();
        this.user = user;
        this.messageUpdatedAtDate = messageUpdatedAtDate;
        this.channelId = channelId;
        this.postType = postType;
        this.fileId = fileId;
        this.fileThumbnailUrl = fileThumbnailUrl;
    }

    public Message(String id, String subject, String postContent, User user, Date messageUpdatedAtDate, String channelId, String postType, String fileId) {
        this.id = id;
        this.subject = subject;
        this.postContent = postContent;
        this.user = user;
        this.messageUpdatedAtDate = messageUpdatedAtDate;
        this.channelId = channelId;
        this.postType = postType;
        this.fileId = fileId;
    }

    public String getId() {
        return id;
    }

    @Override
    public String getText() {
        return postContent;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPostContent() {
        return postContent;
    }

    public void setPostContent(String postContent) {
        this.postContent = postContent;
    }

    public User getUser() {
        return user;
    }

    @Override
    public Date getCreatedAt() {
        return messageUpdatedAtDate;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getMessageUpdatedAtDate() {
        return messageUpdatedAtDate;
    }

    public void setMessageUpdatedAtDate(Date messageUpdatedAtDate) {
        this.messageUpdatedAtDate = messageUpdatedAtDate;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getPostType() {
        return postType;
    }

    public void setPostType(String postType) {
        this.postType = postType;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getFileThumbnailUrl() {
        return fileThumbnailUrl;
    }

    public void setFileThumbnailUrl(String fileThumbnailUrl) {
        this.fileThumbnailUrl = fileThumbnailUrl;
        this.postContent = "FILE: "+postContent;
    }

    @Nullable
    @Override
    public String getImageUrl() {
        return fileThumbnailUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.postContent);
        dest.writeParcelable(this.user, flags);
        dest.writeString(this.subject);
        dest.writeLong(this.messageUpdatedAtDate != null ? this.messageUpdatedAtDate.getTime() : -1);
        dest.writeString(this.channelId);
        dest.writeString(this.postType);
        dest.writeString(this.fileId);
        dest.writeString(this.fileThumbnailUrl);
    }

    private Message(Parcel in) {
        this.id = in.readString();
        this.postContent = in.readString();
        this.user = in.readParcelable(User.class.getClassLoader());
        this.subject = in.readString();
        long tmpMessageUpdatedAtDate = in.readLong();
        this.messageUpdatedAtDate = tmpMessageUpdatedAtDate == -1 ? null : new Date(tmpMessageUpdatedAtDate);
        this.channelId = in.readString();
        this.postType = in.readString();
        this.fileId = in.readString();
        this.fileThumbnailUrl = in.readString();
    }

    public static final Creator<Message> CREATOR = new Creator<Message>() {
        @Override
        public Message createFromParcel(Parcel source) {
            return new Message(source);
        }

        @Override
        public Message[] newArray(int size) {
            return new Message[size];
        }
    };

    @Override
    public String toString() {
        return "Message{" +
                "id='" + id + '\'' +
                ", postContent='" + postContent + '\'' +
                ", user=" + user +
                ", subject='" + subject + '\'' +
                ", messageUpdatedAtDate=" + messageUpdatedAtDate +
                ", channelId='" + channelId + '\'' +
                ", postType='" + postType + '\'' +
                ", fileId='" + fileId + '\'' +
                ", fileThumbnailUrl='" + fileThumbnailUrl + '\'' +
                '}';
    }
}
