package io.github.abhishekwl.alapp.Activities;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.github.abhishekwl.alapp.Models.Event;
import io.github.abhishekwl.alapp.Models.User;
import io.github.abhishekwl.alapp.Models.Venue;
import io.github.abhishekwl.alapp.R;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class AddEventActivity extends AppCompatActivity {

    @BindView(R.id.addEventsTitleLinearLayout)
    LinearLayout addEventsTitleLinearLayout;
    @BindView(R.id.addEventsEventNameEditText)
    TextInputEditText addEventsEventNameEditText;
    @BindView(R.id.addEventsEventDescriptionEditText)
    TextInputEditText addEventsEventDescriptionEditText;
    @BindView(R.id.addEventsVenueSpinner)
    Spinner addEventsVenueSpinner;
    @BindView(R.id.addEventsStartDateAndTimeTextView)
    TextView addEventsStartDateAndTimeTextView;
    @BindView(R.id.addEventsEndDateAndTimeTextView)
    TextView addEventsEndDateAndTimeTextView;
    @BindView(R.id.addEventsAddEventButton)
    Button addEventsAddEventButton;
    @BindString(R.string.base_server_url)
    String baseServerUrl;

    private Unbinder unbinder;
    private User currentUser;
    private String token;
    private OkHttpClient okHttpClient;
    private Calendar calendar;
    private int currentYear;
    private int currentMonth;
    private int currentDate;
    private int currentHour;
    private int currentMin;
    private int currentSec;
    private int eventStartHour;
    private int eventStartMinute;
    private int eventStartYear;
    private int eventStartMonth;
    private int eventStartDayOfMonth;
    private int eventEndHour;
    private int eventEndMinute;
    private int eventEndYear;
    private int eventEndMonth;
    private int eventEndDayOfMonth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_event);

        unbinder = ButterKnife.bind(AddEventActivity.this);
        initializeComponents();
        initializeViews();
    }

    private void initializeComponents() {
        currentUser = getIntent().getParcelableExtra("USER");
        token = getIntent().getStringExtra("TOKEN");
        okHttpClient = new OkHttpClient();
        calendar = Calendar.getInstance();
        currentYear = calendar.get(Calendar.YEAR);
        currentMonth = calendar.get(Calendar.MONTH);
        currentDate = calendar.get(Calendar.DAY_OF_MONTH);
        currentHour = calendar.get(Calendar.HOUR);
        currentMin = calendar.get(Calendar.MINUTE);
        currentSec = calendar.get(Calendar.SECOND);
    }

    private void initializeViews() {
        initializeSpinner();
    }

    @OnClick(R.id.addEventsStartDateAndTimeTextView)
    public void onStartDateAndTimeClick() {
        TimePickerDialog timePickerDialog = new TimePickerDialog(AddEventActivity.this, (view, hourOfDay, minute) -> {
            String startTime = " "+hourOfDay+":"+minute;
            eventStartHour = hourOfDay;
            eventStartMinute = minute;
            addEventsStartDateAndTimeTextView.setText(addEventsStartDateAndTimeTextView.getText().toString().concat(startTime));
        }, currentHour, currentMin, false);
        DatePickerDialog datePickerDialog = new DatePickerDialog(AddEventActivity.this, (view, year, month, dayOfMonth) -> {
            String dateOfEvent = "From\n"+year+"/"+month+"/"+dayOfMonth;
            eventStartYear = year;
            eventStartMonth = month;
            eventStartDayOfMonth = dayOfMonth;
            addEventsStartDateAndTimeTextView.setText(dateOfEvent);
            timePickerDialog.show();
        }, currentYear, currentMonth, currentDate);
        datePickerDialog.show();
    }


    @OnClick(R.id.addEventsEndDateAndTimeTextView)
    public void onEndDateAndTimeClick() {
        TimePickerDialog timePickerDialog = new TimePickerDialog(AddEventActivity.this, (view, hourOfDay, minute) -> {
            String endTime = " "+hourOfDay+":"+minute;
            eventEndHour = hourOfDay;
            eventEndMinute = minute;
            addEventsEndDateAndTimeTextView.setText(addEventsEndDateAndTimeTextView.getText().toString().concat(endTime));
        }, currentHour, currentMin, false);
        DatePickerDialog datePickerDialog = new DatePickerDialog(AddEventActivity.this, (view, year, month, dayOfMonth) -> {
            String dateOfEvent = "To\n"+year+"/"+month+"/"+dayOfMonth;
            eventEndYear = year;
            eventEndMonth = month;
            eventEndDayOfMonth = dayOfMonth;
            addEventsEndDateAndTimeTextView.setText(dateOfEvent);
            timePickerDialog.show();
        }, currentYear, currentMonth, currentDate);
        datePickerDialog.show();
    }

    private void initializeSpinner() {
        String[] venuesArray = getResources().getStringArray(R.array.venues);
        ArrayAdapter<String> venuesAdapter = new ArrayAdapter<>(AddEventActivity.this, android.R.layout.simple_spinner_item, venuesArray);
        venuesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        addEventsVenueSpinner.setAdapter(venuesAdapter);
    }

    public Date getDate(int year, int month, int day, int hourOfDay, int minute) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.DAY_OF_MONTH, day);
        cal.set(Calendar.HOUR_OF_DAY, hourOfDay);
        cal.set(Calendar.MINUTE, minute);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    @OnClick(R.id.addEventsAddEventButton)
    public void onAddEventButtonPressed() {
        Date eventStartDate = getDate(eventStartYear, eventStartMonth, eventStartDayOfMonth, eventStartHour, eventStartMinute);
        Date eventEndDate = getDate(eventEndYear, eventEndMonth, eventEndDayOfMonth, eventEndHour, eventEndMinute);
        long eventStartUnixTimeStamp = eventStartDate.getTime()/1000;
        long eventEndUnixTimeStamp = eventEndDate.getTime()/1000;
        String eventName = Objects.requireNonNull(addEventsEventNameEditText.getText()).toString();
        String eventDescription = Objects.requireNonNull(addEventsEventDescriptionEditText.getText()).toString();
        String eventVenue = addEventsVenueSpinner.getSelectedItem().toString();

        Venue venue = new Venue(null, eventVenue, eventVenue, eventVenue);
        Event event = new Event();
        event.setName(eventName);
        event.setDescription(eventDescription);
        event.setVenue(venue);
        event.setStartTime(eventStartUnixTimeStamp);
        event.setEndTime(eventEndUnixTimeStamp);
        new UploadEvent().execute(event);
    }

    @SuppressLint("StaticFieldLeak")
    private class UploadEvent extends AsyncTask<Event, Void, Event> {

        @Override
        protected Event doInBackground(Event... events) {
            try {
                Event event = events[0];
                Venue venue = event.getVenue();
                MediaType mediaType = MediaType.parse("application/json; charset=utf-8");
                JSONObject venueJsonToBeAdded = new JSONObject();
                venueJsonToBeAdded.put("name", venue.getName());
                venueJsonToBeAdded.put("display_name", venue.getName());
                venueJsonToBeAdded.put("description", venue.getDescription());
                venueJsonToBeAdded.put("type", venue.getType());
                Request venueRequest = new Request.Builder()
                        .addHeader("Authorization", "Bearer "+token)
                        .post(RequestBody.create(mediaType, venueJsonToBeAdded.toString()))
                        .url(baseServerUrl+"/venue/add")
                        .build();
                Response venueResponse = okHttpClient.newCall(venueRequest).execute();
                JSONObject venueJson = new JSONObject(Objects.requireNonNull(venueResponse.body()).string());
                String venueId = venueJson.getString("id");
                venue.setId(venueId);
                JSONObject eventJsonToBeAdded = new JSONObject();
                eventJsonToBeAdded.put("event_name", event.getName());
                eventJsonToBeAdded.put("venue_id", venueId);
                eventJsonToBeAdded.put("start_time", event.getStartTime());
                eventJsonToBeAdded.put("end_time", event.getEndTime());
                eventJsonToBeAdded.put("description", event.getDescription());
                eventJsonToBeAdded.put("type", event.getType());
                Request eventRequest = new Request.Builder()
                        .addHeader("Authorization", "Bearer "+token)
                        .post(RequestBody.create(mediaType, eventJsonToBeAdded.toString()))
                        .url(baseServerUrl+"/event/add")
                        .build();
                Response eventResponse = okHttpClient.newCall(eventRequest).execute();
                JSONObject eventResponseJson = new JSONObject(Objects.requireNonNull(eventResponse.body()).string());
                String eventChannelId = eventResponseJson.getString("event_channel_id");
                String organizerId = eventResponseJson.getString("organizer_id");
                Request joinChannelRequest = new Request.Builder()
                        .addHeader("Authorization", "Bearer "+token)
                        .url(baseServerUrl+"/event/"+eventChannelId+"/attend")
                        .build();
                Response channelResponse = okHttpClient.newCall(joinChannelRequest).execute();
                Log.v("CHANNEL_RESPONSE", Objects.requireNonNull(channelResponse.body()).string());
                return event;
            } catch (IOException | JSONException e) { return null; }
        }

        @Override
        protected void onPostExecute(Event event) {
            super.onPostExecute(event);
            Calendar beginTime = Calendar.getInstance();
            beginTime.set(eventStartYear, eventStartMonth, eventStartDayOfMonth, eventStartHour, eventStartMinute);

            Calendar endTime = Calendar.getInstance();
            endTime.set(eventEndYear, eventEndMonth, eventEndDayOfMonth, eventEndHour, eventEndMinute);

            Intent intent = new Intent(Intent.ACTION_EDIT);
            intent.setType("vnd.android.cursor.item/event");
            intent.putExtra(CalendarContract.Events.TITLE, event.getName());
            intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, beginTime.getTimeInMillis());
            intent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endTime.getTimeInMillis());
            intent.putExtra(CalendarContract.Events.ALL_DAY, false);// periodicity
            intent.putExtra(CalendarContract.Events.DESCRIPTION,event.getDescription());
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        unbinder.unbind();
        super.onDestroy();
    }
}
