package io.github.abhishekwl.alapp.Fragments;


import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.github.abhishekwl.alapp.Adapters.NoticesRecyclerViewAdapter;
import io.github.abhishekwl.alapp.R;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class TodoFragment extends Fragment {

    @BindView(R.id.noticesTopLinearLayout)
    LinearLayout noticesTopLinearLayout;
    @BindView(R.id.noticesRecyclerView)
    RecyclerView noticesRecyclerView;
    @BindString(R.string.base_server_url)
    String baseServerUrl;
    @BindString(R.string.batman_notices)
    String noticeUrl;

    private ArrayList<String> noticesArrayList = new ArrayList<>();
    private View rootView;
    private Unbinder unbinder;
    private OkHttpClient okHttpClient;
    private NoticesRecyclerViewAdapter noticesRecyclerViewAdapter;

    public TodoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_todo, container, false);
        unbinder = ButterKnife.bind(TodoFragment.this, rootView);
        initializeComponents();
        initializeViews();
        return rootView;
    }

    private void initializeComponents() {
        noticesRecyclerViewAdapter = new NoticesRecyclerViewAdapter(noticesArrayList);
        okHttpClient = new OkHttpClient();
    }

    private void initializeViews() {
        initializeRecyclerView();
        new FetchNotices().execute();
    }

    private void initializeRecyclerView() {
        noticesRecyclerView.setLayoutManager(new LinearLayoutManager(rootView.getContext()));
        noticesRecyclerView.setHasFixedSize(true);
        noticesRecyclerView.setItemAnimator(new DefaultItemAnimator());
        noticesRecyclerView.setAdapter(noticesRecyclerViewAdapter);
    }

    @SuppressLint("StaticFieldLeak")
    private class FetchNotices extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                Request noticeRequest = new Request.Builder()
                        .url(noticeUrl+"/notice")
                        .build();
                Response noticeResponse = okHttpClient.newCall(noticeRequest).execute();
                String noticeResponseString = Objects.requireNonNull(noticeResponse.body()).string();
                JSONArray noticeJsonArray = new JSONArray(noticeResponseString);
                noticesArrayList.clear();
                for (int i=0; i<noticeJsonArray.length(); i++) noticesArrayList.add(noticeJsonArray.getJSONObject(i).getString("Content"));
            } catch (IOException | JSONException ignored) { }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            noticesRecyclerViewAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        unbinder = ButterKnife.bind(TodoFragment.this, rootView);
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }
}
