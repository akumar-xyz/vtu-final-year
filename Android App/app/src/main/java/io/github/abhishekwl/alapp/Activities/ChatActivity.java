package io.github.abhishekwl.alapp.Activities;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.stfalcon.chatkit.messages.MessageInput;
import com.stfalcon.chatkit.messages.MessagesList;
import com.stfalcon.chatkit.messages.MessagesListAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.github.abhishekwl.alapp.Models.Channel;
import io.github.abhishekwl.alapp.Models.Message;
import io.github.abhishekwl.alapp.Models.User;
import io.github.abhishekwl.alapp.R;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;

public class ChatActivity extends AppCompatActivity {

    @BindView(R.id.chatChannelNameTextView)
    TextView chatChannelNameTextView;
    @BindView(R.id.chatMessagesListView)
    MessagesList chatMessagesListView;
    @BindView(R.id.chatMessageInput)
    MessageInput chatMessageInput;
    @BindView(R.id.chatProgressBar)
    ProgressBar progressBar;
    @BindString(R.string.base_server_url)
    String baseServerUrl;
    @BindString(R.string.global_team_id)
    String globalTeamId;

    private Unbinder unbinder;
    private User currentUser;
    private Channel currentChannel;
    private String token;
    private OkHttpClient okHttpClient;
    private MessagesListAdapter<Message> messageMessagesListAdapter;
    private MainWebSocketListener mainWebSocketListener;
    private WebSocket webSocket;
    private static final int PICKFILE_REQUEST_CODE = 907;
    private MaterialDialog materialDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        unbinder = ButterKnife.bind(ChatActivity.this);
        initializeComponents();
        initializeViews();
    }

    public Bitmap StringToBitMap(String encodedString){
        try {
            byte [] encodeByte=Base64.decode(encodedString,Base64.DEFAULT);
            return BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
        } catch(Exception e) {
            e.getMessage();
            return null;
        }
    }

    private void initializeComponents() {
        currentUser = getIntent().getParcelableExtra("USER");
        currentChannel = getIntent().getParcelableExtra("CHANNEL");
        token = getIntent().getStringExtra("TOKEN");
        messageMessagesListAdapter = new MessagesListAdapter<>(currentUser.getId(), (imageView, url, payload) -> {
            Bitmap imageBitmap = StringToBitMap(url);
            if (imageBitmap!=null) Glide.with(getApplicationContext()).load(imageBitmap).into(imageView);
        });
        okHttpClient = new OkHttpClient();
        Request webSocketRequest = new Request.Builder()
                .addHeader("Authorization", "Bearer "+token)
                .url(baseServerUrl+"/websocket")
                .build();
        mainWebSocketListener = new MainWebSocketListener();
        webSocket = okHttpClient.newWebSocket(webSocketRequest, mainWebSocketListener);
        //okHttpClient.dispatcher().executorService().shutdown();
        messageMessagesListAdapter.setOnMessageClickListener(message -> {
        });
        messageMessagesListAdapter.setOnMessageLongClickListener(message -> {

        });
    }

    private void initializeViews() {
        if (currentChannel.getType().equalsIgnoreCase("D")) chatChannelNameTextView.setText("@".concat(currentChannel.getDisplayName()));
        else chatChannelNameTextView.setText("#".concat(currentChannel.getDisplayName()));
        chatMessagesListView.setAdapter(messageMessagesListAdapter);
        new FetchPostsInChannel().execute(currentChannel.getId());
        chatMessageInput.setInputListener(input -> {
            new CreateNewPostInChannel().execute(input.toString());
            return true;
        });
        chatMessageInput.setAttachmentsListener(() -> {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("file/*");
            startActivityForResult(intent, PICKFILE_REQUEST_CODE);
        });
    }

    @SuppressLint("StaticFieldLeak")
    private class CreateNewPostInChannel extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... strings) {
            try {
                MediaType mediaType = MediaType.parse("application/json; charset=utf-8");
                JSONObject requestBodyJson = new JSONObject();
                requestBodyJson.put("channel_id", currentChannel.getId());
                requestBodyJson.put("message", strings[0]);
                Request createPostRequest = new Request.Builder()
                        .addHeader("Authorization", "Bearer "+token)
                        .post(RequestBody.create(mediaType, requestBodyJson.toString()))
                        .url(baseServerUrl+"/posts")
                        .build();
                okHttpClient.newCall(createPostRequest).execute();
                return null;
            } catch (JSONException | IOException ignored) { return null; }
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class FetchPostsInChannel extends AsyncTask<String, Void, ArrayList<Message>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected ArrayList<Message> doInBackground(String... strings) {
            try {
                ArrayList<Message> messageArrayList = new ArrayList<>();
                String channelId = strings[0];
                Request postsRequest = new Request.Builder()
                        .addHeader("Authorization", "Bearer "+token)
                        .url(baseServerUrl+"/channels/"+channelId+"/posts")
                        .build();
                Response postsResponse = okHttpClient.newCall(postsRequest).execute();
                JSONObject rootPostsJson = new JSONObject(Objects.requireNonNull(postsResponse.body()).string());
                JSONArray orderJsonArray = rootPostsJson.getJSONArray("order");
                JSONObject allPostsJson = rootPostsJson.getJSONObject("posts");
                for (int i=0; i<orderJsonArray.length(); i++) {
                    String postId = orderJsonArray.getString(i);
                    JSONObject postJson = allPostsJson.getJSONObject(postId);
                    String postAuthorId = postJson.getString("user_id");
                    String postContent = postJson.getString("message");
                    long updatedAt = postJson.getLong("update_at");
                    Date updatedAtDate = new Date(updatedAt);
                    String postType = postJson.getString("type");
                    User postAuthor = getUserFromUserId(postAuthorId);
                    postContent = "["+postAuthor.getUserName()+"]\n\n"+postContent;
                    Message message = new Message(postId, postContent, postAuthor, updatedAtDate, channelId, postType, null, null);
                    if (postJson.has("file_ids")) {
                        JSONArray fileIdsJson = postJson.getJSONArray("file_ids");
                        String fileId = fileIdsJson.getString(0);
                        String fileThumbnailBitmap = fetchFilePreviewFromFileId(fileId);
                        message.setFileId(fileId);
                        message.setFileThumbnailUrl(fileThumbnailBitmap);
                    }
                    messageArrayList.add(message);
                }
                return messageArrayList;
            } catch (IOException | JSONException ignored) { }
            return null;
        }

        @Override
        protected void onPostExecute(ArrayList<Message> messages) {
            super.onPostExecute(messages);
            progressBar.setVisibility(View.GONE);
            messageMessagesListAdapter.addToEnd(messages, false);
        }
    }

    private User getUserFromUserId(String uid) throws IOException, JSONException {
        Request userRequest = new Request.Builder()
                .addHeader("Authorization", "Bearer "+token)
                .url(baseServerUrl+"/users/"+uid)
                .build();
        Response userResponse = okHttpClient.newCall(userRequest).execute();
        JSONObject userJson = new JSONObject(Objects.requireNonNull(userResponse.body()).string());
        String username = userJson.getString("username");
        userRequest = new Request.Builder()
                .addHeader("Authorization", "Bearer "+token)
                .url(baseServerUrl+"/users/"+uid+"/image")
                .build();
        userResponse = okHttpClient.newCall(userRequest).execute();
        Bitmap userImageBitmap = BitmapFactory.decodeStream(Objects.requireNonNull(userResponse.body()).byteStream());
        return new User(uid, username, userImageBitmap);
    }

    private class MainWebSocketListener extends WebSocketListener {
        private static final int NORMAL_CLOSURE_STATUS = 1000;

        @Override
        public void onOpen(WebSocket webSocket, Response response) {
            super.onOpen(webSocket, response);
            try {
                Log.v("WEBSOCKET_OPEN", Objects.requireNonNull(response.body()).string());
            } catch (IOException ignored) { }
        }

        @Override
        public void onMessage(WebSocket webSocket, String text) {
            super.onMessage(webSocket, text);
            Log.v("WEBSOCKET_RECV", text);
            handleNewMessage(text);
        }

        @Override
        public void onClosing(WebSocket webSocket, int code, String reason) {
            super.onClosing(webSocket, code, reason);
            webSocket.close(NORMAL_CLOSURE_STATUS,null);
            Log.v("WEBSOCKET_CLOSE",reason);
        }

        @Override
        public void onFailure(WebSocket webSocket, Throwable t, Response response) {
            super.onFailure(webSocket, t, response);
            Log.v("WEBSOCKET_FAIL", t.getMessage());
        }
    }

    private void handleNewMessage(String text) {
        try {
            JSONObject messageJson = new JSONObject(text);
            String event = messageJson.getString("event");
            if (event.equalsIgnoreCase("posted") && messageJson.getJSONObject("broadcast").getString("channel_id").equals(currentChannel.getId())) {
                String postString = messageJson.getJSONObject("data").getString("post");
                JSONObject postJson = new JSONObject(postString);

                String postId = postJson.getString("id");
                String channelId = postJson.getString("channel_id");
                String postAuthorId = postJson.getString("user_id");
                String postContent = postJson.getString("message");
                long updatedAt = postJson.getLong("update_at");
                Date updatedAtDate = new Date(updatedAt);
                String postType = postJson.getString("type");
                User postAuthor = getUserFromUserId(postAuthorId);
                postContent = "["+postAuthor.getUserName()+"]\n\n"+postContent;
                Message message = new Message(postId, postContent, postAuthor, updatedAtDate, channelId, postType, null, null);
                if (postJson.has("file_ids")) {
                    JSONArray fileIdsJson = postJson.getJSONArray("file_ids");
                    String fileId = fileIdsJson.getString(0);
                    String fileThumbnailBitmap = fetchFilePreviewFromFileId(fileId);
                    message.setFileId(fileId);
                    message.setFileThumbnailUrl(fileThumbnailBitmap);
                }
                runOnUiThread(() -> messageMessagesListAdapter.addToStart(message, true));
            }
        } catch (JSONException | IOException ignored) { }
    }

    public String BitMapToString(Bitmap bitmap){
        if (bitmap==null) return null;
        else {
            ByteArrayOutputStream baos = new  ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG,100, baos);
            byte [] b=baos.toByteArray();
            return Base64.encodeToString(b, Base64.DEFAULT);
        }
    }

    private String fetchFilePreviewFromFileId(String fileId) throws IOException {
        Request fileThumbnailRequest = new Request.Builder()
                .addHeader("Authorization", "Bearer "+token)
                .url(baseServerUrl+"/files/"+fileId+"/thumbnail")
                .build();
        Response fileThumbnailResponse = okHttpClient.newCall(fileThumbnailRequest).execute();
        Bitmap fileThumbnailBitmap = BitmapFactory.decodeStream(Objects.requireNonNull(fileThumbnailResponse.body()).byteStream());
        return BitMapToString(fileThumbnailBitmap);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==PICKFILE_REQUEST_CODE && resultCode==RESULT_OK && data!=null && data.getData()!=null) {
            Uri selectedFileUri = data.getData();
            new uploadFileFromCurrentUserAccount().execute(selectedFileUri);
        }
    }

    private String getRealPathFromURI(Context context, Uri contentUri) {
        String[] proj = { MediaStore.Images.Media.DATA };
        try (Cursor cursor = context.getContentResolver().query(contentUri, proj, null, null, null)) {
            int column_index = Objects.requireNonNull(cursor).getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } catch (Exception e) {
            Log.v("getRealPathFromURI", e.toString());
            return "";
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class uploadFileFromCurrentUserAccount extends AsyncTask<Uri, Void, String> {

        @Override
        protected String doInBackground(Uri... uris) {
            try {
                File fileToBeUploaded = new File(getRealPathFromURI(getApplicationContext(), uris[0]));
                ContentResolver contentResolver = getApplicationContext().getContentResolver();
                String mimeType = contentResolver.getType(uris[0]);
                RequestBody fileUploadRequestBody = new MultipartBody.Builder()
                        .setType(MultipartBody.FORM)
                        .addFormDataPart("channel_id", currentChannel.getId())
                        .addFormDataPart("files", uris[0].getLastPathSegment(), RequestBody.create(MediaType.parse((Objects.requireNonNull(mimeType))), fileToBeUploaded))
                        .build();
                Request fileUploadRequest = new Request.Builder()
                        .addHeader("Authorization", "Bearer "+token)
                        .addHeader("X-Requested-With", "XMLHttpRequest")
                        .post(fileUploadRequestBody)
                        .url(baseServerUrl+"/files")
                        .build();
                Response fileUploadResponse = okHttpClient.newCall(fileUploadRequest).execute();
                JSONObject fileUploadResponseJson = new JSONObject(Objects.requireNonNull(fileUploadResponse.body()).string());
                JSONObject fileInfoJson = fileUploadResponseJson.getJSONArray("file_infos").getJSONObject(0);
                String fileId = fileInfoJson.getString("id");
                String fileName = fileInfoJson.getString("name");

                MediaType mediaType = MediaType.parse("application/json; charset=utf-8");
                JSONObject requestBodyJson = new JSONObject();
                requestBodyJson.put("channel_id", currentChannel.getId());
                requestBodyJson.put("message", fileName);
                requestBodyJson.put("file_ids", new JSONArray().put(fileId));
                Request createPostRequest = new Request.Builder()
                        .addHeader("Authorization", "Bearer "+token)
                        .post(RequestBody.create(mediaType, requestBodyJson.toString()))
                        .url(baseServerUrl+"/posts")
                        .build();
                okHttpClient.newCall(createPostRequest).execute();
            } catch (IOException | JSONException ignored) { return null; }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }
    }

    @OnClick(R.id.chatSearchImageView)
    public void onSearchImageViewButtonPressed() {
    }

    @Override
    protected void onDestroy() {
        unbinder.unbind();
        super.onDestroy();
    }
}
